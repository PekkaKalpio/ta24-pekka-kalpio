import express from 'express'
import "dotenv/config";


const server = express()

server.get('/', (request, response) => {
	response.send('Hello from docker')
})

server.get('/country', (request, response) => {
	response.send(process.env.COUNTRY)
})

server.get('/version', (request, response) => {
	response.send(process.env.VERSION)
})

server.get('/secret', (request, response) => {
	response.send(process.env.SECRET)
})

server.listen(3000, () => {
	console.log('API running @ 3000')
})
