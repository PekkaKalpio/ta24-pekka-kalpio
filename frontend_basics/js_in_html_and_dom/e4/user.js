let username = "Default name";
let textLabel = document.getElementById("welcome_text");
const nameForm = document.getElementById("nameform");

const hello = (p) => {
	textLabel.innerText = `Welcome ${p}!`;
};

onload = () => hello(username);
nameForm.addEventListener("submit", (e) => {
	e.preventDefault();
	username = document.querySelector("#name").value;
	hello(username);
});
