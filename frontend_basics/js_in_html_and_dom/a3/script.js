let seconds = 0;
let minutes = 0;
const counter = document.getElementById("secondsCounter");


onload = () => {
	counter.innerHTML = `seconds: ${seconds}`;
	setInterval(countDown, 1000);
}

const countDown = () => {
	seconds++;
	if (seconds >= 60) {
		seconds = 0;
		minutes++;
	}
	if (minutes === 0) {
		counter.innerHTML = `seconds: ${seconds}`;
	} else {
		counter.innerHTML = `minutes: ${minutes} : seconds: ${seconds}`;
	}
}