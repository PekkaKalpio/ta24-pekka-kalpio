const posts = [{ name: "John", text: "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur" }, { name: "Julia", text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum." }];
const postForm = document.getElementById("postForm");
const grid = document.getElementById("postGrid");


postForm.addEventListener("submit", (e) => {
	e.preventDefault();
	const name = document.querySelector("#name").value;
	const text = document.querySelector("#postText").value;
	const postItem = document.createElement("div");
	postItem.classList.add("border-bottom");
	postItem.innerHTML = `<div class="flexbox"><h2 class="fs-2">${name}</h2> <button class="btn btn-danger btn-small" id="deleteButton" onclick="deletethis(this.parentElement)">Delete</button></div>`;
	postItem.innerHTML += text;
	grid.appendChild(postItem);
});


onload = () => {
	posts.forEach(post => {
		const postItem = document.createElement("div");
		postItem.classList.add("border-bottom");
		postItem.innerHTML = `<div class="flexbox"><h2 class="fs-2">${post.name}</h2> <button class="btn btn-danger btn-small" id="deleteButton" onclick="deletethis(this.parentElement.parentElement)">Delete</button></div>`;
		postItem.innerHTML += post.text;
		grid.appendChild(postItem);
	});
};

const deletethis = (buttonParent) => {
	buttonParent.remove();
}