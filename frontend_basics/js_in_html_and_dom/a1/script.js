const students = [ { name: "Sami", score: 24.75 },
	{ name: "Heidi", score: 20.25 },
	{ name: "Jyrki", score: 27.5 },
	{ name: "Helinä", score: 26.0 },
	{ name: "Maria", score: 17.0 },
	{ name: "Yrjö", score: 14.5  } ];

const button = document.getElementById("togglebutton");
const tableBody = document.getElementById("tablebody");

button.onclick = () => {
	if(tableBody.childNodes[1]){
		tableBody.innerHTML = "";
	}else {
		let index = 1;
		students.forEach(student => {
			tableBody.innerHTML += `
	<tr>
		<th scope="row">${index}</th>
		<td>${student.name}</td>
		<td>${student.score}</td>
	</tr>
	`;
	index++;
		});
	}
};