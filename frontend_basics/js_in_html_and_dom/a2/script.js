const books = [{ name: "dune", pageCount: 712 }, { name: "The Eye of The World", pageCount: 782 }];
const bookForm = document.getElementById("bookForm");
const grid = document.getElementById("bookGrid");


bookForm.addEventListener("submit", (e) => {
	e.preventDefault();
	const bookName = document.querySelector("#bookName").value;
	const pageCount = document.querySelector("#pageCount").value;
	const bookItem = document.createElement("div");
	bookItem.classList.add("p-2", "border-top");
	bookItem.innerHTML = `${bookName} (${pageCount} pages)`;
	grid.appendChild(bookItem);
});


onload = () => {
	books.forEach(book => {
		const bookItem = document.createElement("div");
		bookItem.classList.add("p-2", "border-top");
		bookItem.innerHTML = `${book.name} (${book.pageCount} pages)`
		grid.appendChild(bookItem);
	});
};