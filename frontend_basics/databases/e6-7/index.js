import express from "express";
import { createProductsTable } from "./db.js";
import dao from "./dao.js"

const server = express();
server.use(express.json());

createProductsTable();

server.post("/", async (req, res) => {
	const product = req.body;
	const result = await dao.insertProduct(product);
	const storedProduct = { id: result.rows[0], ...product };
	res.send(storedProduct);
})

server.get("/", async (_req, res) => {
	const result = await dao.findAll();
	res.send(result.rows);
})

server.get("/:id", async (req, res) => {
	const result = await dao.findOne(req.params.id);
	res.send(result.rows[0]);
})

server.put("/:id", async (req, res) => {
	const product = { id: req.params.id, ...req.body };
	const result = await dao.updateProduct(product);
	res.send(product);
})

server.delete("/:id", async (req, res) => {
	const id = req.params.id;
	const result = await dao.deleteById(id);
	res.status(200).send('Deleted');
})

const { PORT } = process.env;
server.listen(PORT, () => {
	console.log("Products API listening to port", PORT);
});
