import { v4 as uuidv4 } from "uuid";
import executeQuery from "./db.js";


const insertProduct = async (product) => {
	const id = uuidv4();
	const params = [id, ...Object.values(product)];
	console.log(params);
	console.log(`Inserting a new product ${params}`);
	const query = `INSERT INTO products (id, name, price) VALUES ($1, $2, $3);`;
	const result = await executeQuery(query, params);
	console.log(`product ${id} inserted successfully`);
	return result;
}

const findAll = async () => {
	console.log("requesting all products");
	const query = `SELECT * FROM products;`;
	const result = await executeQuery(query);
	console.log(`found ${result.rows.length} products`);
	return result;
}

const findOne = async (id) => {
	console.log(`requesting one products from ${id}`);
	const params = [id];
	const query = `SELECT * FROM products WHERE id = $1;`;
	const result = await executeQuery(query, params);
	console.log(`found ${result.rows.length} products`);
	return result;
}


const deleteById = async (id) => {
	console.log(`Deleting products with id from ${id}`);
	const params = [id];
	const query = `DELETE FROM products WHERE id = $1;`;
	const result = await executeQuery(query, params);
	return result;
}


const updateProduct = async (product) => {
	console.log(`updating product ${product.id}`);
	const params = [product.id, product.name, product.price];
	const query = `UPDATE products SET name = $2, price = $3 WHERE id = $1;`;
	const result = await executeQuery(query, params);
	console.log(`product ${product.id} updated succesfully`);
	return result;
}

export default { insertProduct, findAll, findOne, deleteById, updateProduct };