import HelloWorld from "./Hello.jsx";
import Names from "./Names.jsx";


const App = () => {
	const year = new Date().getFullYear();
	const namelist = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"];
	return (
	<div>
		<HelloWorld name="React" year={year}/>
		<Names nameArray={namelist} />
	</div>
	)
};



export default App;