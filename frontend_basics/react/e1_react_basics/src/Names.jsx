const Names = ({ nameArray }) => {
	let cursive = false;
	const namesDOM = nameArray.map(element => 
		{if(cursive){
			cursive = false;
			return <><i key={element}>{element}</i><br/></>
		}else {
			cursive = true;
			return <><b key={element}>{element}</b><br/></>
		}}
	);
	return (<>{namesDOM}</>);

}

export default Names;