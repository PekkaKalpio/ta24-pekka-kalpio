import Year from "./Year.jsx";

const HelloWorld = ({ name, year }) => {
	return (
		<div>
			<h1>Hello {name}</h1>
			<Year year={year} />
		</div>
	)
}

export default HelloWorld;