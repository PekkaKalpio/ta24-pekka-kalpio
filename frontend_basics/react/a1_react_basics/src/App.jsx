import Greetings from "./Greetings";
import R2D2 from "./R2D2.jsx";
import Planets from "./Planets.jsx";

const App = () => {
	const fullname = "Testi Testinen";
	const age = 33;

	const planetList = [
		{ name: "Hoth", climate: "Ice" },
		{ name: "Tattooine", climate: "Desert" },
		{ name: "Alderaan", climate: "Temperate" },
		{ name: "Mustafar", climate: "Volcanic", }
	];

	return (
	<div>
		<Greetings fullname={fullname} age={age} />
		<R2D2 />
		<Planets planetList={planetList}/>
	</div>
	)
};



export default App;