import "./App.css";

const R2D2 = () => {

	return (
		<div className="r2d2">
			<div className="box">
				<img className="r2pic" src="https://gitea.buutti.com/education/academy-assignments/raw/commit/ffb2a11e6ae00a03807ef1e1fa14141395ffda74/React/1.%20React%20Basics/r2d2.jpg"></img>
				<h2 className="centeredText">Hello, i am R2D2!</h2>
				<i className="centeredText">BeeYoop BeeDeepBoom Weeop DEEpaEEya!</i>
			</div>
		</div>
	)
}
export default R2D2;