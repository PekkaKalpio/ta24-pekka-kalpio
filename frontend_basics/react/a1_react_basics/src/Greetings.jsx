const Greetings = ({ fullname, age }) => {
	return (
		<div>
			<p>Hello my name is {fullname} and my age is: {age}.</p>
		</div>
	)
}

export default Greetings;