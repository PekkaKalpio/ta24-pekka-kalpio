const Planets = ({planetList}) => {

	const listedPlanets = planetList.map(planet => {
		return <tr key={planet.name}><td >{planet.name}</td><td>{planet.climate}</td></tr>;
	});
	return (
		<table>
			<tbody>
				{listedPlanets}
			</tbody>
		</table>
	)
};

export default Planets;