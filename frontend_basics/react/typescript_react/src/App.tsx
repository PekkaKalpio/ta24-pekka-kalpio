import Exercise2Component from "./Exercise2Component";
const numero1:number = 3;
const numero2:number = 124;

const App = () => (
	<div>
		<Exercise2Component teksti="Testilause" numero={numero1} />
		<br />
		<Exercise2Component teksti="Testilause" numero={numero2} />
	</div>
);

  
export default App;
