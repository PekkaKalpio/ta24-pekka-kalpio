const Exercise2Component = ({ teksti ,numero }:PorpsInterface):JSX.Element => {
	
	const flipWordBackwards = (word:string):string => {
		const newWord: Array<string> = [];

		for (let index = word.length; index >= 0; index--) {
			newWord.push(word[index]);
		}
		return newWord.join("");
	};
	
	if(numero % 2 === 0){
		return <i>{teksti}</i>;
	}else {
		const backwards = flipWordBackwards(teksti);
		return <b>{backwards}</b>;
	}
};

interface PorpsInterface {
	teksti:string;
	numero:number;
}

export default Exercise2Component;