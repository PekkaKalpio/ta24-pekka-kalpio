import { useState } from "react";
import { TodoNote } from "./TodoNote";
import "./app.css";

class Todo {
	id: number;
	text: string;
	complete: boolean;
	constructor(text: string){
		this.id = 1;
		this.text = text;
		this.complete = false;
	}
}


function App() {
	const [todo, setTodo] = useState<Array<Todo>>([
		{id: 1, text: "Buy potatoes", complete: false},
		{id: 2, text: "Make food", complete: false},
		{id: 3, text: "Exercise", complete: false},
		{id: 4, text: "Do the dishes", complete: false},
		{id: 5, text: "Floss the teeth", complete: false},
		{id: 6, text: "Play videogames", complete: true},
	]);

	const toggleComplete = (id: number) => {
		const newNotes = [...todo];

		newNotes.map(element => {
			id === element.id ? element.complete ?
				element.complete = false : element.complete = true : element;
		});

		setTodo(newNotes);
	};

	const editNoteText = (id: number, editText: string) => {
		const newNotes = [...todo];

		newNotes.map(element => {
			if(id === element.id){
				element.text = editText;
			} 
			return element;
		});

		setTodo(newNotes);
	};

	const makeTodoNotes = todo.map(element=> {
		return <TodoNote key={element.id} id={element.id} text={element.text} complete={element.complete} 
			toggleComplete={toggleComplete} editNoteText={editNoteText}/>;
	});
	
		
	
	return (
		<>
			{makeTodoNotes}
		</>
	);
}

export default App;
