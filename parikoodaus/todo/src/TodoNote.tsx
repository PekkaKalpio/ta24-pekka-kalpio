import { useState } from "react";

export function TodoNote({id, text, complete, toggleComplete, editNoteText}: ValidateProps) {
	const [isEditing, setIsEditing] = useState(false);
	const [editText, setEditText] = useState(text);
	const handleClick = () => {
		toggleComplete(id);
	};

	const onEdit = () => {
		if(isEditing) {
			editNoteText(id, editText);
		}
		isEditing ? setIsEditing(false) : setIsEditing(true);
	};

	const onTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setEditText(event.target.value);
	};

	return (
		
		<div className={complete ? "complete" : ""}>
			{isEditing ? <input type="textarea" value={editText} onChange={onTextChange}></input> : 
				<p>{text}</p>}
			<input type="checkbox" checked={complete} onChange={handleClick}></input>
			<button onClick={onEdit}>{isEditing ? "save" : "edit"}</button>
		</div>

	);
}

interface ValidateProps{
	id: number,
	text: string,
	complete: boolean,
	toggleComplete: (id: number) => void,
	editNoteText: (id: number, editText: string) => void
}