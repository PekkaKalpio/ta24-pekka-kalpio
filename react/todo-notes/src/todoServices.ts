import axios from "axios";
import { Todo } from "./App.tsx";

const url = "http://localhost:3000/todos";

const getAll = () => {
	return axios.get(url);
};

const postTodo = (newTodo:Todo) => {
	return axios.post(url, newTodo);
};

const updateTodo = (id:string, newTodo:Todo | undefined) => {
	return axios.put(`${url}/${id}`, newTodo);
};

const deleteTodo = (id:string) => {
	return axios.delete(`${url}/${id}`);
};

export { getAll, postTodo, updateTodo, deleteTodo };