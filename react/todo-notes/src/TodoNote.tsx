import { useState } from "react";

export const TodoNote = ( { id, text, complete, toggleCompletetion, deleteNote, editNoteText}:PropsValidation ) => {
	const [isEditing, setIsEditing] = useState(false);
	const [editText, setEditText] = useState(text);

	const checkHanlder = () => {
		toggleCompletetion(id);
	};

	const deleteHandler = () => {
		deleteNote(id);
	};

	const edithandler = () => {
		if(isEditing){
			editNoteText(id, editText);
		}
		isEditing ? setIsEditing(false) : setIsEditing(true);

	};

	const onTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setEditText(event.target.value);
	};


	return (
		<div className={complete ? "todoNote todoNoteComplete" : "todoNote"}>
			<input type="checkbox" className="form-check-input todoNoteItem checkBox" checked={complete} onChange={checkHanlder}></input>
			{isEditing ? <input type="text" value={editText} onChange={onTextChange} className="todoNoteItem text"></input> : <p className="todoNoteItem text">{text}</p>}
			<button className={complete ? "btn btn-light todoNoteItem editButton" : "btn btn-outline-primary todoNoteItem editButton"} onClick={edithandler}>{isEditing ? "SAVE" : "EDIT" }</button>
			<button className="btn btn-danger todoNoteItem deleteButton" onClick={deleteHandler}>DELETE</button>
		</div>
	);
};

interface PropsValidation {
	id: string,
	text: string,
	complete: boolean,
	toggleCompletetion: ( id: string ) => void,
	deleteNote: ( id: string ) => void,
	editNoteText: ( id: string, text: string ) => void
}