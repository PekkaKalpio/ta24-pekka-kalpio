import { useEffect, useState } from "react";
import "./App.css";
import { TodoNote } from "./TodoNote";
import "bootstrap/dist/css/bootstrap.min.css";
import { v4 as uuidv4 } from "uuid";
import { getAll, postTodo, updateTodo, deleteTodo } from "./todoServices.ts";
import "./App.scss";

export class Todo {
	id: string;
	text: string;
	complete: boolean;
	constructor (text: string) {
		this.id = uuidv4();
		this.text = text;
		this.complete = false;		
	}
}

function App() {
	const [todos, setTodos] = useState<Array<Todo>>([]);

	async function hook() {
		const response = await getAll();
		console.log(response.data);
		setTodos(response.data);
	}

	useEffect( () => {hook();}, []);

	const [newNoteText, setNewNoteText] = useState("");
	const [filterText, setFilterText] = useState("");
	const [isFiltered, setIsFiltered] = useState(false);
	
	const addNewNote = async ():Promise<void> => {
		const newNote = new Todo(newNoteText);
		try {
			await postTodo(newNote);
			setTodos( [ ...todos, newNote ] );
			setNewNoteText("");
		} catch (error) {
			console.log(error);
		}
	};

	const onTextChange = (event: React.ChangeEvent<HTMLInputElement>):void => {
		setNewNoteText(event.target.value);
	};

	const onFilterTextChange = (event: React.ChangeEvent<HTMLInputElement>):void => {
		setFilterText(event.target.value);
	};
	
	const toggleFilter = ():void => isFiltered ? setIsFiltered(false) : setIsFiltered(true);

	const makeNotesDependingOnFilter = ():Array<JSX.Element> => {
		if(isFiltered){
			const filteredTodos = todos.filter(element => element.text.toLowerCase().includes(filterText.toLowerCase()));
			const makeNotes = filteredTodos.map (element => {
				return premadeTodoNote(element.id, element.text, element.complete);
			});
			return makeNotes;
		}  else {
			const makeNotes = todos.map (element => {
				return premadeTodoNote(element.id, element.text, element.complete);
			});
			return makeNotes;
		}
	
	};

	const premadeTodoNote = (id: string, text: string, complete: boolean) => <TodoNote key={id} id={id} text={text} complete={complete} 
		toggleCompletetion={toggleCompletetion} deleteNote={deleteNote} editNoteText={editNoteText}/>;

	const toggleCompletetion = async (id: string):Promise<void> => {
		const toggledCompleteTodos = todos.map( element => {
			if( element.id === id ){
				element.complete ? element.complete = false : element.complete = true;
				
			}
			return element;
		});
		try {
			const updatedTodoNote = todos.find(element => element.id === id);
			await updateTodo(id, updatedTodoNote);
			setTodos(toggledCompleteTodos);
		} catch (error){
			console.log(error);
		}
	};

	const deleteNote = async ( id: string ):Promise<void> => {
		const todosFilteredToDelete = todos.filter( element => element.id !== id );
		try {
			await deleteTodo(id);
			setTodos(todosFilteredToDelete);
		}catch(error){
			console.log(error);
		}
	};

	const editNoteText = async (id: string, text: string ):Promise<void> => {
		let edited = true;
		const todosWithEditedText = todos.map (element => {
			if( element.id === id ){
				if (element.text === text){
					edited = false;
				}
				element.text = text;
			}
			return element;
		});
		if(!edited){
			return;
		}
		try {
			const updatedTodoNote = todos.find(element => element.id === id);
			await updateTodo(id, updatedTodoNote);
			setTodos(todosWithEditedText);
		} catch (error){
			console.log(error);
		}
	};

	const makeNotes = makeNotesDependingOnFilter();
	
	return (
		<>
			<div className="container">
				<div className="titleContainer">
					<h1 id="title">Todo notes (bootstrap edition)</h1>
					<button onClick={addNewNote} className="btn btn-primary btn-small">Add a new note</button>
					<input type="text" className="textInput" onChange={onTextChange} value={newNoteText}></input>
					
					<button onClick={toggleFilter} className="btn btn-primary btn-small">{isFiltered ? "Stop filtering": "Filter notes" } </button>
					{isFiltered ? <input type="text" className="textInput" onChange={onFilterTextChange} value={filterText}></input> : "" }

				</div>
				<div className="todoContainer">
					{makeNotes}
				</div>
			</div>
		</>
	);
}

export default App;
