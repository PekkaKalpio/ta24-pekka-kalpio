import { useState } from "react";
import "./App.css";
import DisappearingButton from "./DisappearingButton";
import "bootstrap/dist/css/bootstrap.min.css";
import Bingo from "./Bingo.tsx";


function App() {
	const [hide, sethide] = useState(false);
	const hideText = () => {
		hide ? sethide(false) : sethide(true);
	};

	const names: Array<string> = [
		"Anakin Skywalker",
		"Leia Organa",
		"Han Solo",
		"C-3PO",
		"R2-D2",
		"Darth Vader",
		"Obi-Wan Kenobi",
		"Yoda",
		"Palpatine",
		"Boba Fett",
		"Lando Calrissian",
		"Jabba the Hutt",
		"Mace Windu",
		"Padmé Amidala",
		"Count Dooku",
		"Qui-Gon Jinn",
		"Aayla Secura",
		"Ahsoka Tano",
		"Ki-Adi-Mundi",
		"Luminara Unduli",
		"Plo Koon",
		"Kit Fisto",
		"Shmi Skywalker",
		"Beru Whitesun",
		"Owen Lars" ];
	
	return (
		<div className="container">
			<div>
				<h1>Assignment 1</h1>
				<button onClick={() =>{hideText();}} className="btn btn-primary">clicky</button>
				{!hide && <p>This text disappears if you click the button</p>}

				<h1>Assignment 2</h1>
				<DisappearingButton buttonNumber={1} />
				<DisappearingButton buttonNumber={2} />
				<DisappearingButton buttonNumber={3} />
				<DisappearingButton buttonNumber={4} />
				<DisappearingButton buttonNumber={5} />
			</div>
			<div>
				<h1>Assignment 3</h1>
				<Bingo names={names} />

			</div>
		</div>
	);
}

export default App;
