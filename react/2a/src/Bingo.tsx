import { useState } from "react";
import BingoButton from "./BingoButton.tsx";

const Bingo = ({names}:PropsValidation) => {
	const [bingo, setBingo] = useState(false);
	const [bingoRows, setBingoRows] = useState([
		[false, false ,false ,false ,false],
		[false, false ,false ,false ,false],
		[false, false ,false ,false ,false],
		[false, false ,false ,false ,false],
		[false, false ,false ,false ,false]
	]);
	
	let count = 5;
	let arrayCount = 0;
	const nameRows:Array<Array<string>> = [[],[],[],[],[]];
	for (let index = 0; index < names.length; index++) {
		nameRows[arrayCount].push(names[index]);
		count--;
		if(count === 0){
			count = 5;
			arrayCount++;
		}
	}

	const handleClick = (row:number, column:number, clickState:boolean) => {
		const newBingo = bingoRows;
		newBingo[row][column] = clickState;
		setBingoRows(newBingo);
		checkBingo();
	};

	const checkBingo = () => {
		let isBingo = false;
		for (let index = 0; index < 5; index++) {
			if(bingoRows[index][0] && bingoRows[index][1] && bingoRows[index][2] && bingoRows[index][3] && bingoRows[index][4] ){
				isBingo = true;
			}
		}
		for (let index = 0; index < 5; index++) {
			if(bingoRows[0][index] && bingoRows[1][index] && bingoRows[2][index] && bingoRows[3][index] && bingoRows[4][index] ){
				isBingo = true;
			}
		}
		if(bingoRows[0][0] && bingoRows[1][1] && bingoRows[2][2] && bingoRows[3][3] && bingoRows[4][4] ||
			bingoRows[0][4] && bingoRows[1][3] && bingoRows[2][2] && bingoRows[3][1] && bingoRows[4][0]
		){
			isBingo = true;
		}
		if(isBingo){
			setBingo(true);
		}else if (bingo && !isBingo){
			setBingo(false);
		}
	};
	
	const makeButtons1 = nameRows[0].map((element, index) => {
		return	<BingoButton key={element} name={element} row={0} column={index} handleClick={handleClick} />;
	});
	const makeButtons2 = nameRows[1].map((element, index) => {
		return	<BingoButton key={element} name={element} row={1} column={index} handleClick={handleClick} />;
	});
	const makeButtons3 = nameRows[2].map((element, index) => {
		return	<BingoButton key={element} name={element} row={2} column={index} handleClick={handleClick} />;
	});
	const makeButtons4 = nameRows[3].map((element, index) => {
		return	<BingoButton key={element} name={element} row={3} column={index} handleClick={handleClick} />;
	});
	const makeButtons5 = nameRows[4].map((element, index) => {
		return	<BingoButton key={element} name={element} row={4} column={index} handleClick={handleClick} />;
	});

	return (
		<>
			{bingo && 
				<h1> BINGO!!! </h1>
			}
			<div className="flexBox">{makeButtons1}</div>
			<br />
			<div className="flexBox">{makeButtons2}</div>
			<br />
			<div className="flexBox">{makeButtons3}</div>
			<br />
			<div className="flexBox">{makeButtons4}</div>
			<br />
			<div className="flexBox">{makeButtons5}</div>
		</>
	);
};

interface PropsValidation {
	names : Array<string>
}

export default Bingo;