import { useState } from "react";

const BingoButton = ({name, row, column, handleClick}:PropsValidation) => {
	const [clicked, setClicked] = useState(false);

	const clickHandler = () => {
		if(!clicked){
			handleClick(row, column, true);
			setClicked(true);
		}
		else {
			handleClick(row, column, false);
			setClicked(false);
		}
	};

	return (
		<>
			<button className={clicked ?
				"btn btn-primary box":
				"btn btn-outline-primary box"}
			onClick={() => {clickHandler();}}>{name}</button>
		</>
	);
};

interface PropsValidation {
	name:string,
	row:number,
	column: number,
	handleClick: (row:number, column:number, clickState:boolean) => void
}

export default BingoButton;