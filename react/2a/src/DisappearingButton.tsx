import { useState } from "react";

const DisappearingButton = ({buttonNumber}:PropsValidation) => {
	const [hide, setHide] = useState(false);
	const disappear = () => {
		hide ? setHide(false) : setHide(true);
	};

	return (
		<>
			{!hide && <button onClick={() => {disappear();}} className="btn btn-outline-primary">button {buttonNumber}</button>}
		</>
	);
};

interface PropsValidation {
	buttonNumber: number;
}

export default DisappearingButton;