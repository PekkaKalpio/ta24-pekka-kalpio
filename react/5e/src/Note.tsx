export function NoteElement ({id, content, date, important, toggleImportant}:PropsValidation ):JSX.Element {
	return (
		<div className={important ? "note important" : "note"}>
			<p>	{id} </p>  <p>{content}</p>
			<button onClick={ () => {toggleImportant(id);}}> toggle importance </button>
		</div>
	);
}

interface PropsValidation {
	id:number,
	content:string,
	date:string,
	important:boolean,
	toggleImportant: (id:number) => void;
}