import { useState, useEffect } from "react";
import axios from "axios";
import { NoteElement } from "./Note";


class Note {
	id:number;
	content:string;
	date:string;
	important:boolean;
	constructor(id:number, content:string, date:string, important:boolean) {
		this.id = id;
		this.content = content;
		this.date = date;
		this.important = important;
	}
}
export function Notes () {
	const [notes, setNotes] = useState<Note[]>([]);
	const api = "http://localhost:3000/notes";
	
	useEffect(() => async () => {
		console.log("hep");
		const response = await axios.get<Note[]>(api);
		console.log(response);
		setNotes(response.data);
	}, [setNotes]);


	const postToServer = async ( content:string) => {
		const newDate = new Date().toISOString();
		const important = false;
		const newNote = new Note(notes.length+1, content, newDate, important);
		const response = await axios.post(api, newNote);
		setNotes(notes.concat(newNote));
		console.log(response);
	};

	const toggleImportant = async (id: number) => {
		const foundNote = notes.find( element => element.id === id);
		const newImportant = foundNote?.important ? false : true;
		const response = axios.put(`${api}/${id}`, {...foundNote, important: newImportant});
		console.log(response);
	};

	const makeNotes = notes.map(element => {
		return <NoteElement key={element.id} id={element.id} content={element.content} date={element.date} important={element.important} toggleImportant={toggleImportant} />;
	});

	return (
		<div>
			<button onClick={ () => {postToServer("new note");}}>add note</button>
			{makeNotes}
		</div>
	);
}

