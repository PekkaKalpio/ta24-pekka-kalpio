import React, { useState } from "react";
import { Home } from "./Home.tsx";
import { About } from "./About.tsx";
import { Links } from "./Links.tsx";
import "./App.css";
import {
	BrowserRouter as Router,
	Routes, Route, Link
} from "react-router-dom";


function App() {
	/*
	const [page, setPage] = useState("home");



	const content = () => {
		if(page === "home"){
			return <Home />;
		} else if(page === "about"){
			return <About />;
		}else if(page === "links"){
			return <Links />;
		}
	};

	const toPage = ( page: string ) => ( event: React.MouseEvent<HTMLElement>) => {
		event.preventDefault();
		setPage(page);
	}; */

	return (
		<div className="container">
			{/* <div className="linkContainer">
				<h1>E1</h1>
				<a href="" onClick={toPage("home")} >home</a>
				<a href="" onClick={toPage("about")} >about</a>
				<a href="" onClick={toPage("links")} >links</a>
			</div>
			{content()} */}
			<div>
				<h1>E2</h1>
				<Router>
					<div>
						<Link  className="link" to="/">Home </Link>
						<Link  className="link" to="/links">Links </Link>
						<Link  className="link" to="/about">About</Link>
					</div>

					<Routes>
						<Route path="/links" element={<Links />} />
						<Route path="/about" element={<About />} />
						<Route path="/" element={<Home />} />
					</Routes>
				</Router>
			</div>
		</div>

	
	);
}

export default App;
