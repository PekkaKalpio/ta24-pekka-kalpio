import { Notes } from "./Notes.tsx";

export const Home = () => (
	<div>
		<h1>Home</h1>
		<Notes />
	</div>
);
