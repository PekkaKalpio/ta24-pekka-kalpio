import { useState } from "react";

export const CounterButtons3 = () => {
	const [ counters, setCounters ] = useState( [{id:"1", count:0}, {id:"2", count:0}, {id:"3", count:0}] );

	const handleClick = ( numberIndex: number ) :void => { 
		const newCounters = counters.map(element => {
			if(element.id === numberIndex.toString()){
				element.count++;
			}
			return element;
		});
		setCounters( newCounters );
	};

	const counterButtons = counters.map(( element, index ) => {
		return <button key={ index + 1 } onClick={ () => { handleClick( index + 1); }}> { element.count } </button>;
	});

	return (
		<div>
			<h1> Assignment 6 </h1>
			{counterButtons}
			<p> {counters.reduce((acc, cur) => acc + cur.count, 0)} </p>
		</div>
	);
};