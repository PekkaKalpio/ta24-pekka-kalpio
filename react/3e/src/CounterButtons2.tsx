import { useState } from "react";

export const CounterButtons2 = () => {
	const [ counters, setCounters ] = useState( [ 0, 0, 0 ] );

	const handleClick = ( numberIndex: number ) :void => { 
		const newCounters = [ ...counters ];
		newCounters[ numberIndex ]++;
		setCounters( newCounters );
	};

	const counterButtons = counters.map(( _element, index ) => {
		return <button key={index} onClick={() => {handleClick(index); }}> {counters[index]} </button>;
	});

	return (
		<div>
			<h1> Assignment 5 </h1>
			{counterButtons}
			<p> {counters.reduce((cur, acc) => acc+cur, 0)} </p>
		</div>
	);
};