import { useState } from "react";

export const CounterButtons = () => {
	const [ counters, setCounters ] = useState( [ 0, 0, 0, 0 ] );

	const handleClick = ( numberIndex: number ) :void => {
		const newCounters = [ ...counters ];

		newCounters[ numberIndex ]++;
		newCounters[ 3 ]++;

		setCounters( newCounters );
	};


	return (
		<div>
			<h1> Assignment 4 </h1>
			<button onClick={() => {handleClick(0); }}> {counters[0]} </button>
			<button onClick={() => {handleClick(1); }}> {counters[1]} </button>
			<button onClick={() => {handleClick(2); }}> {counters[2]} </button>
			<p> {counters[3]} </p>
		</div>
	);
};