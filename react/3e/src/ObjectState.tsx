import { useState } from "react";

export const ObjectState = () => {
	const [state, setState] = useState({count:0, text:""});

	const handleClick = ():void => {
		setState({ ...state, count: state.count + 1 });
	};

	const handleChange = (event: React.ChangeEvent<HTMLInputElement>):void => {
		setState({ ...state, text: event.target.value});
	};

	return(
		<div>
			<h1>Assignment 3</h1>
			<input value={state.text} onChange={handleChange}></input>
			<button onClick={handleClick}>count: {state.count}</button>
		</div>
	);
};