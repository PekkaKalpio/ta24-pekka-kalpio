export const Counter = ( { id, counter, handleClick ,handleDelete } :PropsValidation ) => {
	return <div key={id} className="counterClass">
		<button  onClick={() => {handleClick(id); }} className="buttonClass counterButton"> {counter} </button>
		<button	onClick={ () => {handleDelete(id);}} className="buttonClass"> delete this counter</button>
	</div>;
};

interface PropsValidation {
	id: string;
	counter: number;
	handleClick: (index:string) => void,
	handleDelete: (index:string) => void
}