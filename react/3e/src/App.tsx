import { useState } from "react";
import { Assignment2 } from "./Assignment2";
import { ObjectState } from "./ObjectState";
import { CounterButtons } from "./CounterButtons";
import { CounterButtons2 } from "./CounterButtons2";
import { CounterButtons3 } from "./CounterButtons3";
import { Counter } from "./Counter";
import { v4 as uuidv4 } from "uuid";
import "./App.scss";

function App() {
	const [text, setText] = useState("text");
	const [finalText, setFinalText] = useState("");
	const [counters, setCounters] = useState( [ { id: "1", count: 0 }, { id: "2", count: 0 }, { id: "3", count: 0 } ] );
	const [minimumCounter, setMinimumCounter] = useState( { id:"min", count : 0 } );
	
	const onTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setText(event.target.value);
	};

	const onMinimumChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setMinimumCounter({...minimumCounter, count: Number(event.target.value)});
	};

	const handleClick = () => {
		setFinalText(text);
		setText("");
	};

	const handleCounterClick = ( id:string ) => {
		const newCount = counters.map(element =>
			element.id === id ?
				{...element, count: element.count + 1 } :
				element
		);
		setCounters(newCount);
	};

	const AddCounter = () => {
		const id = uuidv4();
		setCounters( [...counters, { id:id , count:0 }] );
	};
	const handleDelete = (id:string) => {
		setCounters( counters.filter(element => element.id !== id));
	};
	const filterCounters = counters.filter(element => element.count >= minimumCounter.count);

	const makeCounterComponents =  filterCounters.map(element => {
		return <Counter key={element.id} id={element.id} counter={ element.count } handleClick={ () => { handleCounterClick(element.id);} }  handleDelete={ () => {handleDelete(element.id);} }/>;
	});
	
	return (
		<><div className="countersContainer">
			<h1>Assignment 1</h1>
			<h2>Your string is: {finalText}</h2>
			<input value={text} onChange={onTextChange}></input> <button onClick={handleClick}>submit</button>
		</div>
		<div className="countersContainer">
			<Assignment2 />
		</div>
		<div className="countersContainer">
			<ObjectState />
		</div>
		<div className="countersContainer">
			<CounterButtons />
		</div>
		<div className="countersContainer">
			<CounterButtons2 />
		</div>
		<div className="countersContainer"> 
			<CounterButtons3 />
		</div>
		<div className="countersContainer">
			<h1>Assignment 7</h1>
			<button onClick={AddCounter}>Add Counter</button>
			{makeCounterComponents}
			<p>{counters.reduce((acc, cur) => acc + cur.count, 0)}</p>
		</div>
		<div className="countersContainer">
			<h1>Minimum counter</h1>
			<input type="number" onChange={onMinimumChange} value={minimumCounter.count}></input>
		</div>
		</>
	);
}

export default App;
