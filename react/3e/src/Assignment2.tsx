import { useState } from "react";

export const Assignment2 = () => {
	const [text, setText] = useState("text");
	const [finalText, setFinalText] = useState("");
	
	const onTextChange = (event: React.ChangeEvent<HTMLInputElement>):void => {
		setText(event.target.value);
	};

	const handleClick = (e:React.SyntheticEvent):void => {
		e.preventDefault();
		setFinalText(text);
		setText("");
	};
	
	return (
		<>
			<h1> Assignment 2</h1>
			<h2>Your string is: {finalText}</h2>
			<form onSubmit={handleClick}>
				<input value={text} onChange={onTextChange}></input> 
				<button type="submit">submit</button>
			</form>
		</>
	);
};