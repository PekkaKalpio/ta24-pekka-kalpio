const validateLogin = (request, response, next) => {
	const { username, password } = request.body;
	if (request.method === "POST") {
		if (!username || !password) {
			return response.status(400).send("Missing or invalid parameters");
		}
	}
	next();
};

export default validateLogin;