import { readFileSync, writeFileSync } from "fs";
const fileLocation = "/home/node/todo-data.json";

function readFile() {
	return JSON.parse(readFileSync(fileLocation));
}


function saveFile(data) {
	return writeFileSync(fileLocation, JSON.stringify(data));
}



export { readFile, saveFile }