import express from "express";
import { errorHandler, unknownEndpoint } from "./errorhandler.js";
import todoRouter from "./todoRouter.js";
import cors from "cors";

const server = express();
server.use(express.json());
server.use(express.static("public"));
server.use(cors());

server.get("/", (_request, response) => {
	response.send("ok");
});

server.use("/todos", todoRouter);

server.use(errorHandler);
server.use(unknownEndpoint);

export default server;