const validate = (request, response, next) => {
	const { id, text, complete } = request.body;
	console.log(request.body)
	if (request.method === "POST") {
		if (!id || !text) {
			return response.status(400).send("Missing or invalid parameters");
		}
	}

	next();
};

export default validate;