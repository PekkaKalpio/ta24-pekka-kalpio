import express from "express";
import validate from "./validate.js";
import { readFile, saveFile } from "./todoFileServices.js";

const router = express.Router();
router.use(express.json());
const todos = readFile();


router.get("/", (_request, response) => {
	response.send(todos);
});

router.get("/:id", (request, response) => {
	const student = todos.find(element => element.id === request.params.id);
	if (student) {
		response.send(student);
	}
	else {
		response.status(404).send("student id not found");
	}
});

router.use("/", validate);
router.put("/:id", (request, response) => {
	const { id, text, complete } = request.body;
	let exists = false;
	todos.forEach(element => {
		if (element.id === id) {
			element.text = text;
			element.complete = complete;
			exists = true;
		}
	});
	if (exists) {
		saveFile(todos);
		response.status(200).send("Put successfully");
	} else {
		response.status(404).send("no student with this id");
	}

});

router.delete("/:id", (request, response) => {
	let exists = false;
	for (let index in todos) {
		if (todos[index].id === request.params.id) {
			exists = true;
			todos.splice(index, 1);
		}
	}
	if (exists) {
		saveFile(todos);
		response.status(200).send("Deleted successfully");
	} else {
		response.status(404).send("nothing to delete");
	}
});


router.post("/", (request, response) => {
	todos.push(request.body);
	saveFile(todos);
	response.status(201).send();
});

export default router;