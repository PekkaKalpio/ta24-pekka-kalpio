import "./Person.css";

const Person = ({name, age}:PropsInterface) => <div className="personClass">My name is: {name} and my age: {age}!</div>;

interface PropsInterface{
	name:string,
	age:number
}
export default Person;