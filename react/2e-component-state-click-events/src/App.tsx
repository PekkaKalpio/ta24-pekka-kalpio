import Family from "./Family.tsx";
import "./App.css";
import CounterButton from "./CounterButton.tsx";
import { useState } from "react";

interface person {
	name:string,
	age:number
}

function App() {
	const people: Array<person> = [
		{ name: "Alice", age: 30 },
		{ name: "Bob", age: 35 },
		{ name: "Charlie", age: 40 },
		{ name: "Donna", age: 45 }
	];

	const [hide, setHide] = useState(false);
	const hideStuff = () => {
		hide ? setHide(false) : setHide(true);
	};
	
	const [total, setTotal] = useState(0);

	return (
		<div className="appClass">
			<Family people={people} />
			{hide === false && <>
				<CounterButton setTotal={setTotal} />
				<CounterButton setTotal={setTotal} />
				<CounterButton setTotal={setTotal} />
				<br />
				{total}
			</>}
			<button onClick={ () => {hideStuff();}}>Hide counters</button>
		</div>
	);
}

export default App;
