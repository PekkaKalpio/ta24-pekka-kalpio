import { useState } from "react";

const CounterButton: React.FC<PropsValidation> = ({setTotal}:PropsValidation) => {
	const [counter, setCounter] = useState(0);

	const clickHandler = () => {
		setTotal(total => total +1);
		setCounter(counter + 1);
	};

	return (
		<div>
			<button onClick={ () => {clickHandler();}}>{counter}</button>
		</div>
	);
};

interface PropsValidation {
	setTotal: React.Dispatch<React.SetStateAction<number>>
}

export default CounterButton;