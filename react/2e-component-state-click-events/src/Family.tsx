import Person from "./Person.tsx";
import "./Family.css";

const Family = ( {people}: PropsInterface) => {
	const persons = people.map(person => {
		return <Person key={person.name} name={person.name} age={person.age} />;
	});
	return (
		<> 
			<h3 className="familyClass">Title?!</h3>
			{persons}
		</>
	);
};

interface PropsInterface{
	people: Array<person>;
}

interface person {
	name:string,
	age:number
}

export default Family;