function checkSentenceVowels(sentence) {
    const letters = {
        a:{count:0},
        e:{count:0},
        i:{count:0},
        o:{count:0},
        u:{count:0},
        y:{count:0},
    };
    // Check how many different vowels we have in the sentence

    for (let i = 0; i < sentence.length; i++) {
        switch(sentence.charAt(i).toLowerCase()){
            case "a":
                letters.a.count++;
                break;
            case "e":
                letters.e.count++;
                break;
            case "i":
                letters.i.count++;
                break;
            case "o":
                letters.o.count++;
                break;
            case "u":
                letters.u.count++;
                break;
            case "y":
                letters.y.count++;
                break;
            default:
                break;      
        }
    }

    console.log("A letter count: " + letters.a.count);
    console.log("E letter count: " + letters.e.count);
    console.log("I letter count: " + letters.i.count);
    console.log("O letter count: " + letters.o.count);
    console.log("U letter count: " + letters.u.count);
    console.log("Y letter count: " + letters.y.count);

    const totalCount = letters.a.count+letters.e.count+letters.i.count+letters.o.count+letters.u.count+letters.y.count;

    console.log("Total vowel count: " + totalCount);
}

checkSentenceVowels("A wizard's job is to vex chumps quickly in fog.");
