

const inputCoordinates = (input) => {
    const coordinates = {x:0,y:0};

    for (let i = 0; i < input.length; i++) {
        if(input.charAt(i).toLowerCase()==="c"){
            continue;
        }
        if(input.charAt(i).toLowerCase()==="n"){
            coordinates.y++;
        }
        if(input.charAt(i).toLowerCase()==="s"){
            coordinates.y--;
        }
        if(input.charAt(i).toLowerCase()==="e"){
            coordinates.x++;
        }
        if(input.charAt(i).toLowerCase()==="w"){
            coordinates.x--;
        }
        if(input.charAt(i).toLowerCase()==="b"){
            break;
        }
       
    }
    console.log(`X:${coordinates.x} Y:${coordinates.y}`)
} 
inputCoordinates("NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE");