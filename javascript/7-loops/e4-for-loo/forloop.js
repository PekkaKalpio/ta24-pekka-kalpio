console.log("loop 1")
for(let i=0;i<=1000;i+=100){
    console.log(i);
}

console.log("loop 2")
for(let i=1;i<=128;i*=2){
    console.log(i);
}

console.log("loop 3")
for(let i=3;i<=15;i+=3){
    console.log(i);
}

console.log("loop 4")
for(let i=9;i>=0;i--){
    console.log(i);
}

console.log("loop 5")
let num1 = 1;
for(let i=1;i<12;i++){
    if(i%3===0){
        num1++;
    }
    console.log(num1);
}

console.log("loop 6")
let num2 = 0;
for(let i=0;i<15;i++){
    if(num2>=5){
        num2=0;
    }
    console.log(num2);
    num2++;
}
