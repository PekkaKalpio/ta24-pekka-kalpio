import axios from "axios";

const api = "http://universities.hipolabs.com/search?country=Finland";

const getUniversities = async () => {
	const response = await axios.get(api);
	const names = response.data.map(element => element.name);
	console.log(names);
};

getUniversities();