import axios from "axios";

const api = "https://fakestoreapi.com/products";

const getFakeStoreProducts = async () => {
	const response = await axios.get(api);
	const names = response.data.map(element => element.title);
	console.log(names);
};

getFakeStoreProducts();

const postFakeStoreProduct = async (name, price, description, category) => {
	const newObj =  {
		title: name,
		price: price,
		description: description,
		image: "https://i.pravatar.cc",
		category: category
	};
	const response = await axios.post(api,newObj);
	console.log(response.data);
};

postFakeStoreProduct("test", 400, "lorem ipsum dolor sit amet", "clothing");


const deleteFakeStoreProduct = async () => {
	const response = await axios.delete(`${api}/21`);
	console.log(response.status);
};

deleteFakeStoreProduct();