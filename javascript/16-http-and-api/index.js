import axios from "axios";

// const getPostById = (id) => {
// 	const api = `https://jsonplaceholder.typicode.com/todos/${id}`;
// 	axios.get(api)
// 		.then(response => console.log(`Post# ${response.data.id} by ${response.data.userId} : ${response.data.title}`))
// 		.catch(error => console.log(error));

// };
const api = "https://jsonplaceholder.typicode.com/todos/";

async function getPostById(id) {
	const response = await axios.get(`${api}${id}`);
	console.log(`Post# ${response.data.id} by ${response.data.userId} : ${response.data.title}`);
	return response;
}
//getPostById(2);

async function getPostComments(id) {
	const response = await axios.get(`${api}${id}/comments`,{params:{postId:id}});
	console.log(response.data);
}	

//getPostComments(2);

async function postComment() {
	const post = await axios.post(api, {name:"asd", email:"wasd", body:"dsa"});
	console.log(post.status);
}

//postComment();

async function putComment(id) {
	const response = await getPostById(id);
	console.log(response.data);
	const put = await axios.put("https://jsonplaceholder.typicode.com/posts/" + id, {userId:response.data.userId, id:response.data.id, title:"asdwasd", body:response.data.body});
	console.log(put.status);
}
putComment(2);

