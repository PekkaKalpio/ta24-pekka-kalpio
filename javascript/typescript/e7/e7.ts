const isNumberArray = (a:Array<unknown>):boolean => {
	let isNumber = true;
	if(typeof a === "object"){
		a.forEach(element => {
			if(typeof element !== "number"){
				isNumber = false;
			}	
			});
	} else {
		isNumber = false;
	}
	
	return isNumber;
}

const arr1 = [0,2,4,623,435,234324,6557,86,34543575,5688567,56753,354,4,66,6,6,6,6,6];
const arr2 = [23,4,457,2,5,"k",45,547,2,3];

const arr3 = "hah not an array";
const arr4 = {0:1,1:1,2:1};

console.log(isNumberArray(arr1));
console.log(isNumberArray(arr2));
