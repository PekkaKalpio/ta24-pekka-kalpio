import fs from "fs";
interface Book {
	title:string,
	author:string,
	id:number,
	readingStatus:boolean};
	
let library:Array<Book> = [];

const getBook = (id:number) => {
	const book =  library.filter(element => element.id === id);
	return book[0].title;
 }

 function printBookData (id:number){
	const book =  library.filter(element => element.id === id);
	console.log(book[0]);
 }

 function printReadingStatus (author:string, title:string){
	const book =  library.filter(element => element.author === author && element.title === title);
	console.log(book[0].readingStatus);
 }

 function addNewBook (author:string, title: string) {
	library.push({
		id:library.length+1,
		author: author,
		title: title,
		readingStatus:false
	});
 }

 function readBook (id:number) {
	for(let book of library){
		if (book.id === id) {
			book.readingStatus = true;
		}
	}
 }

 function saveToJSON() {
	fs.writeFileSync("library.JSON", JSON.stringify(library), "utf8");
	console.log("Saved to library.JSON");
 }

 function loadFromJSON() {
	const loadLibrary = fs.readFileSync("library.JSON", "utf8");
	library = JSON.parse(loadLibrary);
	console.log("Loaded from library.JSON");
 }

loadFromJSON();
console.log(getBook(2));
printBookData(1);
printReadingStatus("Harper Lee", "To Kill A Mockingbird");
addNewBook("Mika Waltari", "Sinuhe Egyptian");
console.log(getBook(4));
readBook(3);
printReadingStatus("Harper Lee", "To Kill A Mockingbird");
saveToJSON();