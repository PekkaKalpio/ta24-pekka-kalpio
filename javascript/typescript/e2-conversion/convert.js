var pear = { name: "a pear", weight: 178 };
var lemon = { name: "a lemon", weight: 120 };
var apple = { name: "an apple", weight: 90 };
var mango = { name: "a mango", weight: 150 };
var avgWeight = (pear.weight + lemon.weight + apple.weight + mango.weight) / 4;
console.log(avgWeight);
var dif1 = Math.abs(pear.weight - avgWeight);
var dif2 = Math.abs(lemon.weight - avgWeight);
var dif3 = Math.abs(apple.weight - avgWeight);
var dif4 = Math.abs(mango.weight - avgWeight);
if (dif1 < dif2 && dif1 < dif3 && dif1 < dif4) {
    console.log("".concat(pear.name, " is the closest to average"));
}
if (dif2 < dif1 && dif2 < dif3 && dif2 < dif4) {
    console.log("".concat(lemon.name, " is the closest to average"));
}
if (dif3 < dif2 && dif3 < dif1 && dif3 < dif4) {
    console.log("".concat(apple.name, " is the closest to average"));
}
if (dif4 < dif2 && dif4 < dif3 && dif4 < dif1) {
    console.log("".concat(mango.name, " is the closest to average"));
}
