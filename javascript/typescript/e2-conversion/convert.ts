const pear:{name:string, weight:number} = {name:"a pear", weight:178};
const lemon:{name:string, weight:number} = {name:"a lemon", weight:120};
const apple:{name:string, weight:number} = {name:"an apple", weight:90};
const mango:any = {name:"a mango", weight:150};

const avgWeight:number = (pear.weight+lemon.weight+apple.weight+mango.weight)/4
console.log(avgWeight)

const dif1:number = Math.abs(pear.weight - avgWeight);
const dif2:number = Math.abs(lemon.weight - avgWeight);
const dif3:number = Math.abs(apple.weight - avgWeight);
const dif4:number = Math.abs(mango.weight - avgWeight);

if (dif1<dif2 && dif1 < dif3 && dif1 < dif4){
    console.log(`${pear.name} is the closest to average`)
}

if (dif2<dif1 && dif2 < dif3 && dif2 < dif4){
    console.log(`${lemon.name} is the closest to average`)
}

if (dif3<dif2 && dif3 < dif1 && dif3 < dif4){
    console.log(`${apple.name} is the closest to average`)
}

if (dif4<dif2 && dif4 < dif3 && dif4 < dif1){
    console.log(`${mango.name} is the closest to average`)
}
