const numbers = [ 8, 12, 17, 9, 16, 24, 16, 25, 35, 27, 38, 50 ];

for(num of numbers){
    if(num>20){
        console.log(num);
        break;
    }
}
const findOver20 = numbers.find(num => num>20);

console.log(findOver20);

const over20Index = numbers.findIndex(num => num===findOver20);

console.log(over20Index);

numbers.splice(over20Index);

console.log(numbers);