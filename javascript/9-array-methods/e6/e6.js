const animals = [ "horse", "cow", "dog", "hamster", "donkey", "cat", "parrot" ];

const withO = animals.filter(animal => animal.toLowerCase().includes("o"));

const withoutOorH = animals.filter(animal => !animal.toLowerCase().includes("o") && !animal.toLowerCase().includes("h"));

console.log(withO);
console.log(withoutOorH);