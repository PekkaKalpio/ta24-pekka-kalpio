const students = [ { name: "Sami", score: 24.75 },
                   { name: "Heidi", score: 20.25 },
                   { name: "Jyrki", score: 27.5 },
                   { name: "Helinä", score: 26.0 },
                   { name: "Maria", score: 17.0 },
                   { name: "Yrjö", score: 14.5  } ];

const getGrades = (students) => students.map(student =>  obj = {name:student.name, grade:grading(student.score)});

const grading = (score) => {
    if(score < 14){
        return 0;
    }else if(score >= 14 && score <= 17){
        return 1;
    }else if(score > 17 && score <= 20){
        return 2;
    }else if(score > 20 && score <= 23){
        return 3;
    }else if(score > 20 && score <= 26){
        return 4;
    }else if(score>26){
        return 5;
    }
}

console.log(getGrades(students));