const animals = [ "horse", "cow", "dog", "hamster", "donkey", "cat", "parrot" ];

console.log(animals.find(animal => animal.endsWith("t")));

console.log(animals.find(animal => animal.endsWith("y") && animal.startsWith("d") ));