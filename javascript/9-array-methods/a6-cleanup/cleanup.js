const objectArray = [ { x: 14, y: 21, type: "tree", toDelete: false },
                      { x: 1, y: 30, type: "house", toDelete: false },
                      { x: 22, y: 10, type: "tree", toDelete: true },
                      { x: 5, y: 34, type: "rock", toDelete: true },
                      null,
                      { x: 19, y: 40, type: "tree", toDelete: false },
                      { x: 35, y: 35, type: "house", toDelete: false },
                      { x: 19, y: 40, type: "tree", toDelete: true },
                      { x: 24, y: 31, type: "rock", toDelete: false } ];

const deleteEntries = objectArray.map(element => element === null ? null : element.toDelete? null : element);
console.log(deleteEntries);

for(index in objectArray){
    if(objectArray[index]!=null){
        if(objectArray[index].toDelete){
            objectArray[index]=null;
        }
    }
}
console.log(objectArray)