const total = arr => arr.reduce((acc,cur) => acc + cur);


console.log(total([1,2,3])); // 6

const stringConcat = arr => arr.reduce((acc,cur) => acc.toString() + cur);

console.log(stringConcat([1,2,3])); // "123"

const voters = [
    {name:'Bob' , age: 30, voted: true},
    {name:'Jake' , age: 32, voted: true},
    {name:'Kate' , age: 25, voted: false},
    {name:'Sam' , age: 20, voted: false},
    {name:'Phil' , age: 21, voted: true},
    {name:'Ed' , age:55, voted:true},
    {name:'Tami' , age: 54, voted:true},
    {name: 'Mary', age: 31, voted: false},
    {name: 'Becky', age: 43, voted: false},
    {name: 'Joey', age: 41, voted: true},
    {name: 'Jeff', age: 30, voted: true},
    {name: 'Zack', age: 19, voted: false}
];

const totalVotes = arr =>  arr.reduce((acc,cur) => cur.voted ? acc+=1 : acc, 0);

console.log(totalVotes(voters)); // 7

const wishlist = [
    { title: "Tesla Model S", price: 90000 },
    { title: "4 carat diamond ring", price: 45000 },
    { title: "Fancy hacky Sack", price: 5 },
    { title: "Gold fidgit spinner", price: 2000 },
    { title: "A second Tesla Model S", price: 90000 }
];

const shoppingSpree = arr => arr.reduce((acc, cur) => acc+=cur.price, 0)

console.log(shoppingSpree(wishlist)); // 227005

const arrays = [
    ["1", "2", "3"],
    [true],
    [4, 5, 6]
];

const flatten = arr => arr.reduce((acc, cur) => acc.concat(cur));

console.log(flatten(arrays)); // ["1", "2", "3", true, 4, 5, 6];

// const voters = [
//     {name:'Bob' , age: 30, voted: true},
//     {name:'Jake' , age: 32, voted: true},
//     {name:'Kate' , age: 25, voted: false},
//     {name:'Sam' , age: 20, voted: false},
//     {name:'Phil' , age: 21, voted: true},
//     {name:'Ed' , age:55, voted:true},
//     {name:'Tami' , age: 54, voted:true},
//     {name: 'Mary', age: 31, voted: false},
//     {name: 'Becky', age: 43, voted: false},
//     {name: 'Joey', age: 41, voted: true},
//     {name: 'Jeff', age: 30, voted: true},
//     {name: 'Zack', age: 19, voted: false}
// ];
const voteObj = {
    numYoungVotes:0,
    numYoungPeople:0,
    numMidVotesPeople:0,
    numMidsPeople:0,
    numOldsVotesPeople:0,
    numOldsPeople:0
}
const voterResults = (arr, voteObj) => arr.reduce((acc, cur) => {
    if(cur.age>=18 && cur.age<=25){
        acc.numYoungPeople+=1;
        if(cur.voted){
            acc.numYoungVotes+=1;
        }
    }
    if(cur.age>=26 && cur.age<=35){
        acc.numMidsPeople+=1;
        if(cur.voted){
            acc.numMidVotesPeople+=1;
        }
    }
    if(cur.age>=36 && cur.age<=55){
        acc.numOldsPeople+=1;
        if(cur.voted){
            acc.numOldsVotesPeople+=1;
        }
    }

    return acc;


},voteObj)

console.log(voterResults(voters, voteObj)); // Returned value shown below:
/*
{ numYoungVotes: 1,
  numYoungPeople: 4,
  numMidVotesPeople: 3,
  numMidsPeople: 4,
  numOldVotesPeople: 3,
  numOldsPeople: 4 
}
*/
