const users = [{ firstName: 'Bradley', lastName: 'Bouley', role: 'Full Stack Resident' },
               { firstName: 'Chloe', lastName: 'Alnaji', role: 'Full Stack Resident' },
               { firstName: 'Jonathan', lastName: 'Baughn', role: 'Enterprise Instructor' },
               { firstName: 'Michael', lastName: 'Herman', role: 'Lead Instructor' },
               { firstName: 'Robert', lastName: 'Hajek', role: 'Full Stack Resident' },
               { firstName: 'Wes', lastName: 'Reid', role: 'Instructor'},
               { firstName: 'Zach', lastName: 'Klabunde', role: 'Instructor'}];

const userNames = users.map(user => user.lastName);
console.log(userNames);

const fullStack = users.filter(user => user.role==="Full Stack Resident");
console.log(fullStack);

const instructorNames = users.filter(user => user.role==="Instructor").map(user => `${user.firstName} ${user.lastName}`);
console.log(instructorNames);