const incrementAll = (arr) => {
    const newArr = arr.map(element => element+=1);
    return newArr;
}

const numbers = [ 4, 7, 1, 8, 5 ];
const newNumbers = incrementAll(numbers);
console.log(newNumbers); // prints [ 5, 8, 2, 9, 6 ]

const decrementAll = (arr) => {
    const newArr = arr.map(element => element-=1);
    return newArr;
}

const numbers2 = [ 4, 7, 1, 8, 5 ];
const newNumbers2 = decrementAll(numbers);
console.log(newNumbers2); // prints [ 3, 6, 0, 7, 4 ]