import fs from "node:fs";

interface Forecast {
	day: string,
	temperature: number,
	cloudy: boolean,
	sunny: boolean,
	windy: boolean
} 

const forecast:Forecast =  {
	day: "monday",
	temperature: 20,
	cloudy: true,
	sunny: false,
	windy: false,
};
	
const saveData = JSON.stringify(forecast);
fs.writeFileSync("forecast_data.json", saveData, "utf8");


const readData = fs.readFileSync("forecast_data.json", "utf8");
const newTemperature = JSON.parse(readData);

newTemperature.temperature = -5;
console.log(newTemperature)
const saveNewData = JSON.stringify(newTemperature);
fs.writeFileSync("forecast_data.json", saveNewData, "utf8");
	
