import fs from "node:fs";
let finalString = "";

fs.readFile("./testFile.txt", "utf-8", (err, file) => {
	if(err) {
		console.log(err);
	}else {
		const wordsArray = file.split(" ");
		for(let index in wordsArray){
			if(wordsArray[index].toLowerCase() === "joulu"){
				wordsArray[index] = "kinkku";
			}
			if(wordsArray[index].toLowerCase() === "lapsilla"){
				wordsArray[index] = "poroilla";
			}
		}
		finalString= wordsArray.join(" ").trim();
		console.log(finalString);
		
		fs.writeFile("./writeFile.txt", finalString, (err) => {
			if(err){
				console.log(err);
			}else{
				console.log("success");
			}
		});
	}
});




