import readline from "readline-sync";

let running = true;
let userName = null;
let botName = "Dumb Chat Bot";
let newBotName = null;
const forecast = {};
const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
const date = new Date();

forecast.day = days[date.getDay()];
forecast.temperature = Math.floor(Math.random() * 35);
forecast.cloudy = Boolean(Math.floor(Math.random() * 2));
forecast.sunny = Boolean(Math.floor(Math.random() * 2));
forecast.windy = Boolean(Math.floor(Math.random() * 2));


let answer = readline.question("Hi! I am a dumb chat bot. You can check all the things I do by typing 'help'. : ");

while(running){
	switch(answer.toLowerCase()){
	case "quit":
		running = false;
		break;
	case "hello":
		if (userName != null) {
			answer = readline.question(`Hello ${userName}, did you have other questions? : `);
			userName = null;
		} else {
			console.log(`Hello my name is ${botName}`);
			userName = readline.question("what is your name? : ");
		}
		break;
	case "help":
		console.log(`
		-----------------------------
		Heres a list of commands that I can execute! 

		help: Opens this dialog.
		hello: I will say hello to you
		botInfo: I will introduce myself
		botName: I will tell my name
		botRename: You can rename me
		forecast: I will forecast tomorrows weather 100% accurately
		quit: Quits the program.
		-----------------------------
		: 
		`);
		answer = readline.question("Try something : ");
		break;
	case "botinfo":
		console.log("I am a friendly dumb chat bot, you can ask me only a couple of questions.");
		answer = readline.question("Any other dumb questions? : ");
		break;
	case "botname":
		console.log(`I am ${botName}, you can rename me with botRename`);
		answer = readline.question("Any other dumb questions? : ");
		break;
	case "botrename":
		if(newBotName != null){
			answer = readline.question(`Are you happy with the name ${newBotName}? Yes/No : `);
		} else {
			console.log(`I am currently ${botName}, but you can rename me.`);
			newBotName = readline.question("What do you want to call me? : ");
		}
		break;
	case "forecast":
		console.log(`
		Today is ${forecast.day} and the forecast is ${forecast.temperature} celcius
		windy: ${forecast.windy}, sunny: ${forecast.sunny}, cloudy: ${forecast.cloudy}`);
		answer = readline.question("Any other dumb questions? : ");
		break;
	case "yes": 
		if(newBotName != null){
			botName = newBotName;
			answer = readline.question(`My name is now ${botName}, did you have other questions? : `);
			newBotName = null;
		}else{
			answer = readline.question("I am dumb and don't understand, try 'help'! :");
		}
		break;
	case "no": 
		if(newBotName!= null){
			newBotName = null;
			console.log(`I will keep the name ${botName}`);
			answer = readline.question("Any other dumb questions? : ");
		}else{
			answer = readline.question("I am dumb and don't understand, try 'help'! :");
		}
		break;
	default:
		answer = readline.question("I am dumb and don't understand, try 'help'! :");
		break;
	}
}
