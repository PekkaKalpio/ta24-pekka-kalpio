{

const easyRecursion = (n:number):number => {
    if(n===0){
        return 0;
    }
    if(n===1){
        return 1;
    }else {
        return (easyRecursion(n - 2) * 3) + easyRecursion(n - 1)
    }
    
}
console.log(easyRecursion(17)) ;


}