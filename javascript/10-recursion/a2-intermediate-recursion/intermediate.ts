{

const sentencify = (arr:Array<string>, start:number):string => {
    if(start < arr.length-1){
        return String(arr[start] + " ") + sentencify(arr,start+1);
    }else{
        return String(arr[start] + "!")
    }
}



const wordArray = [ "The", "quick", "silver", "wolf" ];

console.log(sentencify(wordArray, 0)); // prints "The quick silver wolf!"
console.log(sentencify(wordArray, 1)); // prints "quick silver wolf!"
}