class Animal {
    constructor (weight, cuteness) {
        this.weight = weight;
        this.cuteness = cuteness;
    }

    makeSound () {
        console.log("silence");
    }
}

const aminal = new Animal(6.5, 4.0);
aminal.makeSound();
console.log(aminal);

class Cat extends Animal{
    makeSound () {
        console.log("meow");
    }
}

const cat = new Cat(4,8);
cat.makeSound();

class Dog extends Animal{
    constructor (weight, cuteness){
        super(weight, cuteness)
        this.weight = weight;
        this.cuteness = cuteness;
    }
    makeSound () {
        this.cuteness > 4 ? console.log("awoo") : console.log("bark"); 
    }
}

const dog1 = new Dog(7.0, 4.5, "kleinspitz");
const dog2 = new Dog(30.0, 3.75, "labrador");
dog1.makeSound(); // prints "awoo"
dog2.makeSound(); // prints "bark"
