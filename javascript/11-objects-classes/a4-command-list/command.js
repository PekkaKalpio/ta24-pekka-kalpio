const coordinates = {x:0,y:0};

const commandHandlers = {
    N: () => coordinates.y++,
    S: () => coordinates.y--,
    E: () => coordinates.x++,
    W: () => coordinates.x--,
    C: () => null,
    B: () => null
}

const command = (commandList) => {
    for(letter of commandList){
        if(letter.toUpperCase()==="B"){
            return;
        } 
        commandHandlers[letter.toUpperCase()]();
    }
}
const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";
command(commandList);
console.log(coordinates)