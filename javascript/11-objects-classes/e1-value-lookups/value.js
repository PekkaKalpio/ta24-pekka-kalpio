const fruits = {
    banana:118,
    apple:85,
    mango:200,
    lemon:65
}

const printWeight = (fruit) =>{
    if(fruit in fruits){
        console.log(`${fruit} weighs ${fruits[fruit]}`);
    } else {
        console.log(`${fruit} is not supported, supported fruits are ${Object.keys(fruits)}`)
    }
    
}

printWeight("banana");

printWeight("mango");
printWeight("strawberry")