class Rectangle {
    constructor(width, length){
        this.width = width;
        this.length = length;
    }
}

const square = new Rectangle(5,5);
const actualRectangle = new Rectangle(5,10);

console.log(square);
console.log(actualRectangle);