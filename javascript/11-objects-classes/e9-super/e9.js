class Robot{
    constructor(x,y){
        this.x = x;
        this.y = y;
    }
    handleMessages(direction){
        if(direction==="x"){
            this.x++;
        }else if(direction==="y"){
            this.y++;
        }
    }
}

class flexibleRobot extends Robot{
    handleMessages(direction){
        if(direction==="xy" || direction==="yx"){
            this.y++;
            this.x++;
        }else{
            super.handleMessages(direction);
        }
    }
}

const robit = new flexibleRobot(0,0);

robit.handleMessages("xy");
robit.handleMessages("xy");
robit.handleMessages("x");
robit.handleMessages("x");
console.log(robit);