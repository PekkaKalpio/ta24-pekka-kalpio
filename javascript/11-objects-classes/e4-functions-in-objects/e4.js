const supercalculator = { 
    name: "Buutti SuperCalculator 6000",
    cache: "96 GB",
    clockSpeed: 9001.0,
    overclock: function() {this.clockSpeed+=500},
    savePower: function() {if(this.clockSpeed>2000){this.clockSpeed=2000}else{this.clockSpeed=this.clockSpeed/2}}
}

supercalculator.overclock();
console.log(supercalculator);

supercalculator.savePower();
console.log(supercalculator);

supercalculator.savePower();
console.log(supercalculator);
