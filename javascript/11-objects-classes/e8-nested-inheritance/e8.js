class Shape{
    constructor(width, length){
        this.width = width;
        this.length = length;
    }
    getArea() {
        return 0;
    }
}

class Rectangle extends Shape{    
    getArea() {
        return this.width * this.length;
    }
}

class Ellipse extends Shape {
    getArea() {
        return Math.PI * this.width / 2 * this.length /2
    }
}

class Triangle extends Shape{
    getArea(){
        return this.width * this.length /2;
    }
}

class Square extends Rectangle{
    constructor(width){
        super(width);
        this.width=width;
        this.length=width;
    }
}

class Circle extends Ellipse{
    constructor(width){
        super(width);
        this.width=width;
        this.length=width;
    }
}
const square = new Square(5);
const actualRectangle = new Rectangle(5,10);

console.log(square);
console.log(actualRectangle);
console.log(square.getArea());
console.log(actualRectangle.getArea());

const circle = new Circle(5,5);
console.log(circle.getArea())
const triangle = new Triangle(5,5);
console.log(triangle.getArea());