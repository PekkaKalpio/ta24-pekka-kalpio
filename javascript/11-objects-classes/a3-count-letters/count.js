const getCountOfLetters = (str) => {
    const lettersObj = {};
    for(letter of str){
        if(letter != " "){
            lettersObj[letter] ? lettersObj[letter] += 1: lettersObj[letter] = 1;
        }
    }
    return lettersObj;
}

const result = getCountOfLetters("a black cat");
console.log(result);
/* prints 
{
	a: 3,
	b: 1,
	c: 2,
	k: 1,
	l: 1,
	t: 1
}
*/
