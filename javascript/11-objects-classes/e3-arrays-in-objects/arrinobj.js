const student = {
    name:"Aili",
    credits:45,
    courses:[
        {name:"Intro to programming", grade:4 },
        {name:"JavaScript Basics", grade:3 },
        {name: "Functional Programming", grade: 5}
    ]
        
    
}

console.log(`${student.name} got ${student.courses[0].grade} from ${student.courses[0].name}`)

const addCourse = (courseName, courseGrade) => {
    student.courses.push({name:courseName,grade:courseGrade});
}

addCourse("Typescript", 3);
console.log(student)