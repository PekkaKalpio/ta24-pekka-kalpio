class Room{
    constructor(width, height) {
        this.width = width;
        this.height = height;
        this.furniture = [];
    }

    getArea() {
        return this.width * this.height;
    }

    addFurniture(str) {
        this.furniture.push(str);
    }
}

const room = new Room(4.5, 6.0);
console.log(room);

const area = room.getArea();
console.log(area);

room.addFurniture("sofa");
room.addFurniture("bed");
room.addFurniture("chair");
console.log(room);
