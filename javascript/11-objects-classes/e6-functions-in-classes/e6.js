class Rectangle {
    constructor(width, length){
        this.width = width;
        this.length = length;
    }
    getArea() {
        return this.width * this.length;
    }
}

const square = new Rectangle(5,5);
const actualRectangle = new Rectangle(5,10);

console.log(square);
console.log(actualRectangle);
console.log(square.getArea());
console.log(actualRectangle.getArea());
