{
interface Student{
    name:string,
    credits:number,
    courseGrades:CourseGrades;
}
interface CourseGrades{
    "Intro to programming": number;
    "JavaScript Basics": number;
    "Functional Programming": number;
    "Program Design"?:number;
}

const student:Student = {
    name:"Aili",
    credits:45,
    courseGrades:{
        "Intro to programming": 4,
        "JavaScript Basics":3,
        "Functional Programming": 5
    }
}
console.log(student);

student.courseGrades["Program Design"] = 3;
console.log(student);

student.courseGrades["JavaScript Basics"] = 4;
console.log(student)


}