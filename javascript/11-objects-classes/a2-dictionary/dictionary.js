const dictionary = {
hello: "hei",
world: "maailma",
bit: "bitti",
byte: "tavu",
integer: "kokonaisluku",
boolean: "totuusarvo",
string: "merkkijono",
network: "verkko"
}

const printTranslateableWords = () => {
    console.log(`Translateable words are: ${Object.keys(dictionary)}`);
}

printTranslateableWords();

const translate = (str) => {
    if (dictionary[str]) {
        return dictionary[str];
    } else {
        return `No translation exists for word ${str}`;
    };
}


console.log(translate("network"));
console.log(translate("world"));
console.log(translate("asd"));

