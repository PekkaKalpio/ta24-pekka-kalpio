class Robot {
    constructor() {
        this.x = 0;
        this.y = 0;
    }

    handleCommandList (commandList) {
        const commandHandlers = {
            N: () => this.y++,
            S: () => this.y--,
            E: () => this.x++,
            W: () => this.x--,
            C: () => null,
            B: () => null
        }

        for(let letter of commandList){
            if(letter.toUpperCase() === "B"){
                return;
            } 
            commandHandlers[letter.toUpperCase()]();
        }
    }    
}

const ruttunen = new Robot();
ruttunen.handleCommandList("NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE");
console.log(ruttunen);