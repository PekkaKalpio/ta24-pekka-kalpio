const results = {
S:8,
A:6,
B:4,
C:3,
D:2,
F:0,
}

const calculateTotalScore = (grades) => {
    let totalScore = 0;
    const gradeAray = grades.split("");
    gradeAray.forEach(letter => {
        totalScore += results[letter];
    });
    return totalScore;
}

const calculateAverageScore = (grades) => calculateTotalScore(grades) / grades.length;

const totalScore = calculateTotalScore("DFCBDABSB");
console.log(totalScore); // prints 33

const averageScore = calculateAverageScore("DFCBDABSB");
console.log(averageScore); // prints 3.6666666666666665

const gradesArray = [ "AABAACAA", "FFDFDCCDCB", "ACBSABA", "CCDFABABC" ];

const averageArray =gradesArray.map(element => calculateAverageScore(element));
console.log(averageArray);