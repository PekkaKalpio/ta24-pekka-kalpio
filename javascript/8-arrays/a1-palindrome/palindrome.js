const isPalindrome = (p) => {
    const reverseArr = [];

    for(let i=p.length-1;i>=0;i--){
        reverseArr.push(p[i].toLowerCase());
    }

    const reverseText = reverseArr.join("");

    if(reverseText===p.toLowerCase()){
        console.log(`${p} is a palindrome`)
    }else{
        console.log(`${p} is not a palindrome`)
    }
}

isPalindrome("saippuakivikauppias");
isPalindrome("saippuakäpykauppias");

isPalindrome("saippuakivikAuppiAs");