const numberRange = (start, end) => {
    const arr = [];
    if(start<end){
        for(let i=start;i<=end;i++){
            arr.push(i);
        }
    }else {
        for(let i=start;i>=end;i--){
            arr.push(i);
        }
    }
    
    return arr;
}

console.log(numberRange(1,5));
console.log(numberRange(-5,-1));
console.log(numberRange(9,5));