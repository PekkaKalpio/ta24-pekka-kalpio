const findLargest = (arr) => {
    let compare = 0;
    for(item of arr){
        compare = compare < item ? item : compare;
    }
    return compare;
}
const array = [ 4, 19, 7, 1, 9, 22, 6, 13 ];
const largest = findLargest(array);
console.log(largest); // prints 22