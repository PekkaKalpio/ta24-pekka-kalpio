const sortNumberArray = (arr) => {
    const newArr = [];
    for(let i = 0 ; i < arr.length ; i++){
        let compare = arr[0];
        for(item of arr){
            compare = compare > item ? item : compare;
        }
        arr.splice(arr.indexOf(compare),1);
        i--;
        newArr.push(compare);
    }
    return newArr;
}

const array = [ 4, 19, 7, 1, 9, 22, 6, 13 ];
const sortedArray = sortNumberArray(array);
console.log(sortedArray); // prints [ 1, 4, 6, 7, 9, 13, 19, 22 ]
