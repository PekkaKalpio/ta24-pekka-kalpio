const coordinates = {x:0,y:0};


function moveUp(){
    coordinates.y++;
}
function moveDown(){
    coordinates.y--;
}
function moveRight(){
    coordinates.x++;
}
function moveLeft(){
    coordinates.x--;
}
function doNothing(){
    return null;
}

const funcArray = [
    moveUp,
    moveDown,
    moveRight,
    moveLeft,
    doNothing,
    doNothing,
]

const command = (commandList) => {
    let numberCommands = "";
    for(letter of commandList){
        if(letter.toLowerCase()==="n"){
            numberCommands+="0" 
        } 
        if(letter.toLowerCase()==="s"){
            numberCommands+="1"
        }
        if(letter.toLowerCase()==="e"){
            numberCommands+="2"
        }
        if(letter.toLowerCase()==="w"){
            numberCommands+="3"
        }
        if(letter.toLowerCase()==="c"){
            numberCommands+="4"
        }
        if(letter.toLowerCase()==="b"){
            numberCommands+="5"
        } 
    }

    for(number of numberCommands){
        funcArray[Number(number)]();
        if(number==="5"){
            break;
        }
    }
}
const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";
command(commandList);
console.log(coordinates)