const reverseWords = (sentence) => {
    const words = sentence.split(" ");
    for(index in words){
        const word = words[index].split("");
        const reverseWord = [];
        for(let i = word.length; i >= 0; i--){
            reverseWord.push(word[i]);
        }
        words[index] = reverseWord.join("");
    }
    return words.join(" ");
}


const sentence = "this is a short sentence";
const reversed = reverseWords(sentence);
console.log(reversed); // prints "siht si a trohs ecnetnes"