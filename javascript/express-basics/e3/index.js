import express from "express";
import fs from "node:fs";

const server = express();

server.get("/counter", (request, response) => {
	let counter = 0;
	if(request.query.number){
		counter = request.query.number;
	} else{
		counter = readData();
		counter++;	
	}
	console.log(counter);
	saveData(counter);
	response.send(counter.toString());
});


const saveData = (param) => {
	const saveData = JSON.stringify(param);
	fs.writeFileSync("data.json", saveData, "utf8");
};

const readData = () => {
	const readData = fs.readFileSync("data.JSON", "utf8");
	return JSON.parse(readData);
};

server.listen(3000, () => {
	console.log("Listening to port 3000");
});
