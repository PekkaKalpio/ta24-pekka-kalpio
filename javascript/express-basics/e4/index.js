import express from "express";
import fs from "node:fs";

const server = express();
const readData = () => {
	const readData = fs.readFileSync("data.JSON", "utf8");
	return JSON.parse(readData);
};

const counter = readData();

server.get("/counter/:name", (request, response) => {
	const name = request.params.name;
	let count = 0;
	counter.forEach(element => {
		if(element.name === name){
			element.count++;
			count = element.count;
		}
	});
	if(count===0){
		counter.push({name:name,count:0});
	}
	saveData(counter);
	response.send(`${name} count: ${count}`);
});


const saveData = (param) => {
	const saveData = JSON.stringify(param);
	fs.writeFileSync("data.json", saveData, "utf8");
};



server.listen(3000, () => {
	console.log("Listening to port 3000");
});
