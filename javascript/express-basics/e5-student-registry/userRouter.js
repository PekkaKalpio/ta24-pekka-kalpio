import "dotenv/config";
import express from "express";
import argon from "argon2";
import validateLogin from "./validateLogin.js";
import jwt from "jsonwebtoken";

const users = [{ userName: "testUser", password: "$argon2id$v=19$m=65536,t=3,p=4$EqIM6M/94wOFuC0l0DCM0g$vkjKKBxC+I8AeOrUBvR2zSZ2tUZOR42kV+tT88XW5G8" }];
// token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InVzZXIyIiwiaWF0IjoxNzEyODM3NDYzLCJleHAiOjE3MTI4NDQ2NjN9.GsJ_rFkb97r9-xzTm5FsDvP8AqmSgHH1ScARLrQPnSA"

const router = express.Router();
router.use(express.json());

router.use("/", validateLogin);

router.post("/register", async (request, response) => {
	const { username, password } = request.body;
	const hashedPassword = await argon.hash(password);
	users.push({ userName: username, password: hashedPassword });
	const token = createToken(username, "2h");
	console.log(token);
	console.log(hashedPassword);
	response.status(201).send("Registered as " + username);
});

router.post("/login", async (request, response) => {
	const { username, password } = request.body;
	const findUser = users.find(user => user.userName === username);
	if (!findUser) {
		return response.status(401).send("Unauthorized no user");
	}

	try {
		if (await argon.verify(findUser.password, password) === false) {
			return response.status(401).send("Unauthorized wrong pass");
		}
	} catch (err) {
		console.log(err);
		return response.status(401).send("Unauthorized wrong pass");
	}


	const token = createToken(username, "2h");
	console.log(token);
	response.status(204).send();
});


router.post("/admin", async (request, response) => {
	const { username, password } = request.body;
	const adminName = process.env.ADMINNAME;
	const adminPassword = process.env.ADMINPASSWORD;

	const match = await argon.verify(adminPassword, password);
	if (!match || adminName !== username) {
		return response.status(401).send("Unathorized");

	}

	const token = createToken(username, "2h", true);
	response.status(201).send("logged in " + token);
});


const createToken = (username, expiration, admin) => {
	const payload = { username: username };
	if (admin) {
		payload.role = "admin";
	}
	const options = { expiresIn: expiration };

	const token = jwt.sign(payload, process.env.SECRET, options);
	return token;
};

export default router;