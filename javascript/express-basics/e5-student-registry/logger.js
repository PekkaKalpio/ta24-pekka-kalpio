const logs = [];

const logger = (request, response, next) => {
	if(request.method!=="GET"){
		logs.push({
			time: new Date(),
			url: request.url,
			method: request.method,
			body: request.body
		});
	} else{
		logs.push({
			time: new Date(),
			url: request.url,
			method: request.method
		});
	}
	next();
};

export {logger, logs};