import "dotenv/config";
import jwt from "jsonwebtoken";

const authenticate = (request, response, next) => {
	console.log("Authenticating as a user");
	const auth = request.get("Authorization");
	if (!auth?.startsWith("Bearer")) {
		return response.status(401).send("Invalid token");
	}
	const token = auth.substring(7);
	const secret = process.env.SECRET;
	try {
		const decodedToken = jwt.verify(token, secret);
		request.user = decodedToken;
		next();
	} catch (error) {
		return response.status(401).send("Invalid token");
	}
};

const adminAuthenticate = (request, response, next) => {
	console.log("Authenticating as an administrator");
	const auth = request.get("Authorization");
	if (!auth?.startsWith("Bearer")) {
		return response.status(401).send("Invalid token");
	}
	const token = auth.substring(7);
	const secret = process.env.SECRET;
	try {
		const decodedToken = jwt.verify(token, secret);
		console.log(`Token's role is ${decodedToken.role}`);
		if (decodedToken.role !== "admin") {
			return response.status(401).send("Invalid token");
		}
		request.user = decodedToken;
		next();
	} catch (error) {
		return response.status(401).send("Invalid token");
	}
};


export { authenticate, adminAuthenticate };
