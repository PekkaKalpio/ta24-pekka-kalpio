import express from "express";
import validate from "./validate.js";
import { authenticate, adminAuthenticate } from "./authenticate.js";

const router = express.Router();
router.use(express.json());
const students = [];

router.get("/protected", authenticate, (req, response) => {
	response.send(`${req.user.username} accessed protected route`);
});


router.get("/", authenticate, (_request, response) => {
	const ids = students.map(element => element.id);
	response.send(JSON.stringify(ids));
});

router.get("/:id", authenticate, (request, response) => {
	const student = students.find(element => element.id === request.params.id);
	if (student) {
		response.send(JSON.stringify(student));
	}
	else {
		response.status(404).send("student id not found");
	}
});

router.use("/", validate);
router.put("/:id", adminAuthenticate, (request, response) => {
	const { name, email } = request.body;
	let exists = false;
	students.forEach(element => {
		if (element.id === request.params.id) {
			element.name = name;
			element.email = email;
			exists = true;
		}
	});
	if (exists) {
		response.status(200).send("Put successfully");
	} else {
		response.status(404).send("no student with this id");
	}

});

router.delete("/:id", adminAuthenticate, (request, response) => {
	let exists = false;
	for (let index in students) {
		if (students[index].id === request.params.id) {
			exists = true;
			students.splice(index, 1);
		}
	}
	if (exists) {
		response.status(200).send("Deleted successfully");
	} else {
		response.status(404).send("nothing to delete");
	}
});


router.post("/", adminAuthenticate, (request, response) => {
	students.push(request.body);
	response.status(201).send();
});

export default router;