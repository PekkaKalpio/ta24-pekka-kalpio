const validate = (request, response, next) => {
	const { id, name, email } = request.body;
	if (request.method === "POST") {
		if (!name || !email || !id) {
			return response.status(400).send("Missing or invalid parameters");
		}
	} else if (request.method === "PUT") {
		if (!name || !email) {
			return response.status(400).send("Missing or invalid parameters, but for PUT!");
		}
	}

	next();
};

export default validate;