import express from "express";
import { errorHandler, unknownEndpoint } from "./errorhandler.js";
import { logger } from "./logger.js";
import studentRouter from "./studentRouter.js";
import userRouter from "./userRouter.js";

const server = express();
server.use(express.json());
server.use(express.static("public"));

server.get("/", (_request, response) => {
	response.send("ok");
});

server.use("/students", logger);
server.use("/students", studentRouter);

server.use("/", userRouter);

server.use(errorHandler);
server.use(unknownEndpoint);

export default server;