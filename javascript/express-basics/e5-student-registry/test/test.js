import request from "supertest";
import server from "../server.js";


jest.mock("jsonwebtoken", () => ({
	sign: jest.fn().mockReturnValue("mockToken") // Mocking the sign function of jsonwebtoken
}));


describe("POST /login and /register", () => {
	test("valid registration", async () => {
		const response = await request(server)
			.post("/register")
			.send({ username: "user1", password: "password" });

		expect(response.status).toBe(201);
		expect(response.text).toContain("Registered as ");
	});

	test("valid login", async () => {
		const response = await request(server)
			.post("/login")
			.send({ username: "testUser", password: "password" });

		expect(response.status).toBe(204);
	});

	test("invalid username", async () => {
		const response = await request(server)
			.post("/login")
			.send({ username: "wrongUser", password: "password" });

		expect(response.status).toBe(401);
		expect(response.text).toContain("Unauthorized no user");
	});

	test("invalid password", async () => {
		const response = await request(server)
			.post("/login")
			.send({ username: "testUser", password: "wrongPassword" });

		expect(response.status).toBe(401);
		expect(response.text).toContain("Unauthorized wrong pass");
	});
});
