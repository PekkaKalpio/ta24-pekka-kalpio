const errorHandler = (error, request, response, next) => {
	console.log(error + "error??");
	next(error);
};

const unknownEndpoint = (request, response) => {
	response.status(404).send("404 unknown");
};

export { errorHandler, unknownEndpoint };