import express from "express";

const server = express();

server.get("/", (requst, response) => {
	response.send("Hello world");
});

server.get("/endpoint2", (requst, response) => {
	response.send("extra endpoint!?");
});


server.listen(3000, () => {
	console.log("Listening to port 3000");
});
