async function waitFor(milliseconds:number) {
	return new Promise ((resolve) => {
		(setTimeout(() => {
			resolve("hep");
		}, milliseconds));
	});
}

// waitFor(1000).then((val) => {
// 	console.log(val);
// });

async function countToTen() {
	for (let index = 0; index <= 10; index++) {
		console.log(index);
		await waitFor(1000);	
	}
}

countToTen();