const timeoutFunction = (timer:number, func:()=>void):void => {
	console.log("hello");
	setTimeout(() => {
		func();
	}, timer);
};

timeoutFunction(1000, () => timeoutFunction(1000, () => timeoutFunction(1000, () => console.log("last hello"))));