const sum = (limit:number):number => {
	let sum:number = 0;
	for (let index = 0; index <= limit; index++) {
		sum += index;
	}
	return sum;
};

new Promise((resolve) => {
	resolve(sum(5000));
}).then((value) => {
	console.log(value);
});


new Promise((resolve) => {
	setTimeout(() => {
		resolve(sum(5000));
	}, 2500);
}).then((value) => {
	console.log(value);
});


const createDelayedCalculation = (limit:number, milliseconds:number) => new Promise((resolve) =>{
	setTimeout(() => {
		resolve(sum(limit));
	}, milliseconds);
});

// Prints 200000010000000 after a delay of 2 seconds
createDelayedCalculation(20000000, 2000).then(result => console.log(result));

// Prints 1250025000 after a delay of 0.5 seconds
createDelayedCalculation(50000, 500).then(result => console.log(result));
//this one comes first because the functions are ran asyncronously and this one has a horter delay