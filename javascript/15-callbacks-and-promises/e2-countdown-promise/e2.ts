new Promise((resolve, reject)=> {
	resolve("first");
	reject("rejected");
})
	.then((value) => {
		console.log(value);
		setTimeout(() => 
			new Promise((resolve, reject) => {
				resolve("second");
				reject("fail");
			}).then((value) => {
				console.log(value);
				setTimeout(() => 
					new Promise((resolve, reject) => {
						resolve("third");
						reject("rejected3");
					})
						.then((value) => {
							console.log(value);
						}).catch((value) => {
							console.log(value);
						}), 1000);
			}).catch((value) => {
				console.log(value);
			}),1000);
	}).catch((value) => {
		console.log(value);
	});

