interface ReturnValue {
	value:number
}

const getValue = function () {
	return new Promise<ReturnValue>((res) => {
		setTimeout(() => {
			res({ value: Math.random() });
		}, Math.random() * 1500);
	});
};

async function get2Rands() {
	const value1 = await getValue();
	const value2 = await getValue();
	console.log(`${value1.value} ${value2.value}`);
}

get2Rands();

const getValueWithPromise = new Promise((res) => {
	setTimeout(() => {
		res({ value: Math.random() });
	}, Math.random() * 1500);
});

let value1:number = 0;

getValueWithPromise.then((value:ReturnValue) => {
	value1 = value.value;
	getValueWithPromise.then((value:ReturnValue) => {
		console.log(`${value1} ${value.value}`);
	});
});