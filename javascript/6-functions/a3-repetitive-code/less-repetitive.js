const firstTriangle = { width: 7.0, length: 3.5 };
const secondTriangle = { width: 4.3, length: 6.4 };
const thirdTriangle = { width: 5.5, length: 5.0 };

const triangleArea = (triangle) => (triangle.width * triangle.length)/2;

// const largest = (num1,num2,num3) => {
//     if(num1>num2 && num1>num3){
//         console.log("first triangle is the largest");
//     } else if (num2>num1 && num2>num3){
//         console.log("second triangle is the largest");
//     } else if (num3>num1 && num3>num2){
//         console.log("third triangle is the largest");
//     };
// };

// let firstTriangleArea = triangleArea(firstTriangle);
// let secondTriangleArea = triangleArea(secondTriangle);
// let thirdTriangleArea = triangleArea(thirdTriangle);

// console.log("Area of first triangle: " + firstTriangleArea);
// console.log("Area of second triangle: " + secondTriangleArea);
// console.log("Area of third triangle: " + thirdTriangleArea);
// largest(firstTriangleArea,secondTriangleArea,thirdTriangleArea);

const triangleArr = [firstTriangle,secondTriangle,thirdTriangle];
const areaArr = [];
triangleArr.forEach((element) => {
    const area = triangleArea(element);
    areaArr.push(area);
})

const largestTriangle = (areaArr) => {
    const arr = [{name:"first",area:areaArr[0]},{name:"second",area:areaArr[1]},{name:"third",area:areaArr[2]}].sort((a,b) => b.area-a.area);
    console.log(`${arr[0].name} triangle is the largest`)
}

largestTriangle(areaArr);
