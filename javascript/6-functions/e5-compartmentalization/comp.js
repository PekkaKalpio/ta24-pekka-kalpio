const rock = {hp:90};
const tree = {hp:30};

const damageTree = (damage) => {
    tree.hp -= damage;
};
damageTree(15);
const damageRock = (damage) => {
    rock.hp -= damage;
};
damageRock(15);
console.log(tree);
console.log(rock);

const damage = (damage, target) => {
    target.hp -= damage;
}

damage(30,rock);
console.log(rock);
