const arr = [3,4,5];

console.log(arr);

const changeFirst = (arr) => {
    arr[0] += 1;
};
changeFirst(arr);
console.log(arr);

const obj = {name:"test"};
console.log(obj);

const addProperty = (obj) => {
    obj.width = 10;
}
addProperty(obj);
console.log(obj);