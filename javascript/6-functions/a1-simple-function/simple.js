const hello = (lang) => {
    switch(lang){
        case "fi":
            console.log("hei maailma!");
            break;
        case "en":
            console.log("hello world!");
            break;
        case "swe":
            console.log("hej världen!");
            break;
        case "jp":
            console.log("こんにちは世界!");
            break;
        default:
            console.log("not a valid language");
            break;
    }
}

hello("swe");
hello("fi");
hello("jp");