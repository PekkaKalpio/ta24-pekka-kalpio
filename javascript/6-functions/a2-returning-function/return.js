const minimum = (num1,num2,num3) => {
    const arr = [num1,num2,num3].sort((a,b) => a-b);
    return arr[0];
}

console.log(minimum(3,2,1));
console.log(minimum(412,2,45));