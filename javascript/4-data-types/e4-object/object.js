const me = {name: "pekka", age: 29, likesPineappleOnPizza:true} ;

me.age+=1;
me.height = 177;

console.log(typeof(me.name));
console.log(typeof(me.age));
console.log(typeof(me.likesPineappleOnPizza));
console.log(typeof(me.height));

console.log(me);

console.log(typeof(me));