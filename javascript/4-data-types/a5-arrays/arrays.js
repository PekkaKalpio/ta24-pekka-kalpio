const ages = [20,35,27,44];

console.log(ages);

const averageAge = ages.reduce((accumulator, curValue) => {
    return accumulator + curValue
}, 0) / ages.length;

console.log(averageAge)