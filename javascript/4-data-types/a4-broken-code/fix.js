const tree = { x: 6, y: 7, hitpoints: 30 };
const rock = { x: 3, y : 11, hitpoints: 90 };
const damage = 15;

const treeHp = tree.hitpoints;


let treeHitpointsLeft;
let rockHitpointsLeft;

const rockHp = rock.hitpoints;
rockHitpointsLeft = rockHp - damage;

console.log("Rock hitpoints left: " + rockHitpointsLeft);

treeHitpointsLeft = treeHp - damage;

console.log("Tree hitpoints left: " + treeHitpointsLeft);
