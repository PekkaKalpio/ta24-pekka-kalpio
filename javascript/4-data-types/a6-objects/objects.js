const books = {
    book1: {
        name: "Dune",
        pages: 412,
        read: false
    },
    book2: {
        name: "The Eye of the World",
        pages: 412,
        read: true
    }
};
const books2 = [{name:"Dune", pages: 412, read:false},{name:"The Eye of the World", pages:412,read:true}];

console.log(books.book1);
console.log(books.book2);
console.log(books2[1]);
console.log(books2[0]);