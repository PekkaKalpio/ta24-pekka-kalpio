const a = true;
const b = false;

const c = a + b;
const d = 10 + a;
const e = 10 + b;

// c = 1
// d = 11
// e = 10
// boolean true = 1 and false = 0

console.log(c);
console.log(d);
console.log(e);