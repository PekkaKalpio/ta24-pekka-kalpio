const fruits = ["strawberry", "raspberry", "corn", "cucumber", "peanut", "bean"];

fruits[1] = "pumpkin";

console.log(fruits.length);
console.log(fruits);

fruits.push("Chili pepper");

console.log(fruits.length);
console.log(fruits);

fruits.pop();

console.log(fruits.length);
console.log(fruits);

console.log(typeof(fruits));