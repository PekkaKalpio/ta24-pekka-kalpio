let text1 = "something"
let text2 = "foo bar"

console.log(text1 + " " + text2)
// nothing will be printed if i don't assign a value
text2 = "foo"
//as const the value can't change so it will throw an error

console.log(text1 + " " + text2)