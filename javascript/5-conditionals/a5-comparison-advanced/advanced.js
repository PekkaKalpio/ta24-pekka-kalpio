const name1 = "Billy";
const name2 = "Bob";
const name3 = "Franklin";

const names = [name1,name2,name3];

const sorted = names.sort( (a,b) => b.length - a.length);

console.log(`${sorted[0]} ${sorted[1]} ${sorted[2]}`)