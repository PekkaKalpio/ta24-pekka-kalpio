const pear = {name:"a pear", weight:178};
const lemon = {name:"a lemon", weight:120};
const apple = {name:"an apple", weight:90};
const mango = {name:"a mango", weight:150};

const avgWeight = (pear.weight+lemon.weight+apple.weight+mango.weight)/4
console.log(avgWeight)

const dif1 = Math.abs(pear.weight - avgWeight);
const dif2 = Math.abs(lemon.weight - avgWeight);
const dif3 = Math.abs(apple.weight - avgWeight);
const dif4 = Math.abs(mango.weight - avgWeight);

if (dif1<dif2 && dif1 < dif3 && dif1 < dif4){
    console.log(`${pear.name} is the closest to average`)
}

if (dif2<dif1 && dif2 < dif3 && dif2 < dif4){
    console.log(`${lemon.name} is the closest to average`)
}

if (dif3<dif2 && dif3 < dif1 && dif3 < dif4){
    console.log(`${apple.name} is the closest to average`)
}

if (dif4<dif2 && dif4 < dif3 && dif4 < dif1){
    console.log(`${mango.name} is the closest to average`)
}
