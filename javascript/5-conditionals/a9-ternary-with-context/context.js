const computer1 = {time:42, energy:600};
const computer2 = {time:57, energy:480};

const power = (computer2.time*computer2.energy)<(computer1.time*computer1.energy) ? console.log("computer 1 used more power") : console.log("computer 2 used more power");