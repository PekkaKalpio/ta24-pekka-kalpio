const firstWord = "javascript"
const secondWord = "react"

if (firstWord.length>secondWord.length) {
    console.log(`${firstWord} is longer than ${secondWord}`)
} else if (firstWord.length===secondWord.length) {
    console.log("Both words are the same length")
} else {
    console.log(`${secondWord} is longer than ${firstWord}`)
}