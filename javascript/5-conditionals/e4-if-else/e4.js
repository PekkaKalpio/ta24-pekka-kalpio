const bool1 = true;
const bool2 = false;

if (bool1 && bool2){
    console.log("both are true")
} else if (bool1 && !bool2){
    console.log("bool1 is true, bool2 is false")
} else if (!bool1 && bool2){
    console.log("bool1 is false, bool2 is true")
} else {
    console.log("both are false")
};